# KMUtils

[![CI Status](http://img.shields.io/travis/Mauricio Ventura/KMUtils.svg?style=flat)](https://travis-ci.org/Mauricio Ventura/KMUtils)
[![Version](https://img.shields.io/cocoapods/v/KMUtils.svg?style=flat)](http://cocoapods.org/pods/KMUtils)
[![License](https://img.shields.io/cocoapods/l/KMUtils.svg?style=flat)](http://cocoapods.org/pods/KMUtils)
[![Platform](https://img.shields.io/cocoapods/p/KMUtils.svg?style=flat)](http://cocoapods.org/pods/KMUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KMUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KMUtils'
```

## Author

Mauricio Ventura, mabavepe2410@hotmail.com

## License

KMUtils is available under the MIT license. See the LICENSE file for more info.
