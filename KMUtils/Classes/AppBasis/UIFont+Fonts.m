//
//  UIFont+Fonts.m
//  DEQ
//
//  Created by Mauricio Ventura on 30/6/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import "UIFont+Fonts.h"

@implementation UIFont (Fonts)




+(UIFont *)actionButtonFont
{
	return 	[self semiboldAppFontOfSize: 20];
}

+(UIFont *)fieldLblFont
{
	return 	[self semiboldAppFontOfSize: 14];
}

+(UIFont *)fieldInputFont
{
	return 	[self semiboldAppFontOfSize: 18];
}

+(UIFont *)titleFont
{
	return 	[self boldAppFontOfSize: 20];
}
+(UIFont *)controllerTitleFont
{
	return 	[self semiboldAppFontOfSize:17];
}





+(UIFont *)lightAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Light" size:size];
}

+(UIFont *)heavyAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Heavy" size:size];
}

+(UIFont *)regularAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Regular" size:size];
}

+(UIFont *)mediumAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Medium" size:size];
}

+(UIFont *)boldAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Bold" size:size];
}

+(UIFont *)blackAppFontOfSize:(CGFloat)size{
	return 	[UIFont fontWithName:@"SFUIDisplay-Black" size:size];
}

+(UIFont *)ultralightAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Ultralight" size:size];
}

+(UIFont *)thinAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Thin" size:size];
}

+(UIFont *)semiboldAppFontOfSize:(CGFloat)size
{
	return 	[UIFont fontWithName:@"SFUIDisplay-Semibold" size:size];
}








+(NSString *)listFontFamilyWith:(NSString *)query
{
	NSString *families = @"";
	for (NSString *familyName in [UIFont familyNames])
	{
		NSString *fontFamily = @"";
		if ([familyName containsString:query])
		{
			fontFamily = [NSString stringWithFormat:@"<<<<<<Family Name: %@>>>>>>\n", familyName];
			for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName])
			{
				fontFamily = [fontFamily stringByAppendingString:[NSString stringWithFormat:@"\n%@\n", fontName]];
			}
			families = [families stringByAppendingString:[NSString stringWithFormat:@"\n%@", fontFamily]];
		}
	}
	return families;
}

@end
