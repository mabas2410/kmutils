//
//  UIColor+Pantone.m
//  RT Admin
//
//  Created by Mauricio Ventura on 13/10/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "UIColor+Pantone.h"

@implementation UIColor (Pantone)



+(UIColor *)navBarSolidColorWithAlpha:(CGFloat)alpha
{
	return [UIColor colorWith255Red:124 green:70 blue:169 alpha:alpha * 255];
}

+(UIColor *)buttonBlueColor
{
	return [UIColor colorWith255Red:65 green:185 blue:234 alpha:255];
}
+(UIColor *)fieldLblColor
{
	return [UIColor colorWith255Red:144 green:163 blue:162 alpha:255];
}
+(UIColor *)blackTextColor
{
	return [UIColor colorWith255Red:40 green:48	blue:55 alpha:255];
}
+(UIColor *)receivedMessageColor
{
	return [UIColor colorWith255Red:218 green:226	blue:235 alpha:255];
}



#pragma mark - Activity Cell Background Colors According to Type

+(UIColor *)activityType:(NSInteger)type
{
	switch (type)
	{
		case 0:
			return [UIColor colorWith255Red:240 green:131	blue:111 alpha:255];
			break;
		case 1:
			return [UIColor colorWith255Red:252 green:209	blue:97 alpha:255];
			break;
		case 2:
			return [UIColor colorWith255Red:134 green:209	blue:184 alpha:255];
			break;
		case 3:
			return [UIColor colorWith255Red:96 green:198	blue:239 alpha:255];
			break;
		case 4:
			return [UIColor colorWith255Red:113 green:113	blue:168 alpha:255];
			break;
		case 5:
			return [UIColor colorWith255Red:182 green:216	blue:99 alpha:255];
			break;
			
  default:
			return [UIColor colorWith255Red:240 green:131	blue:111 alpha:255];
			break;
	}
}













-(BOOL)contrastWithLightColor
{
	CGColorRef cgColor = [self CGColor];
	size_t numComponents = CGColorGetNumberOfComponents(cgColor);
	
	if (numComponents == 4)
	{
		const CGFloat *components = CGColorGetComponents(cgColor);
		if ((components[0] + components[1] + components[2] + components[3]) / 4 > 0.5)
		{
			return false;
		}
	}
	return true;
}

-(UIColor *)changeAlpha:(CGFloat)alpha
{
	CGColorRef cgColor = [self CGColor];
	size_t numComponents = CGColorGetNumberOfComponents(cgColor);
	
	if (numComponents == 4)
	{
  const CGFloat *components = CGColorGetComponents(cgColor);
		return [UIColor colorWithRed:components[0] green:components[1] blue:components[2] alpha:alpha];
		
	}
	return self;
}

+(UIColor *)colorWith255Red:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha
{
	return [UIColor colorWithRed:red / 255.0 green:green / 255.0 blue:blue / 255.0 alpha:alpha / 100];
}

@end
