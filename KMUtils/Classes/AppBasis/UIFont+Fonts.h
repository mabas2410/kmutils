//
//  UIFont+Appfonts.h
//  DEQ
//
//  Created by Mauricio Ventura on 30/6/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Fonts)


+(UIFont *)actionButtonFont;
+(UIFont *)fieldLblFont;
+(UIFont *)fieldInputFont;
+(UIFont *)titleFont;
+(UIFont *)controllerTitleFont;



+(UIFont *)lightAppFontOfSize:(CGFloat)size;
+(UIFont *)heavyAppFontOfSize:(CGFloat)size;
+(UIFont *)regularAppFontOfSize:(CGFloat)size;
+(UIFont *)mediumAppFontOfSize:(CGFloat)size;
+(UIFont *)boldAppFontOfSize:(CGFloat)size;
+(UIFont *)blackAppFontOfSize:(CGFloat)size;
+(UIFont *)ultralightAppFontOfSize:(CGFloat)size;
+(UIFont *)thinAppFontOfSize:(CGFloat)size;
+(UIFont *)semiboldAppFontOfSize:(CGFloat)size;





+(NSString *)listFontFamilyWith:(NSString *)query;

@end
