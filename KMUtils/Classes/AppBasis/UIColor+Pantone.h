//
//  UIColor+Pantone.h
//  RT Admin
//
//  Created by Mauricio Ventura on 13/10/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Pantone)


+(UIColor *)navBarSolidColorWithAlpha:(CGFloat)alpha;
+(UIColor *)buttonBlueColor;
+(UIColor *)fieldLblColor;
+(UIColor *)blackTextColor;
+(UIColor *)receivedMessageColor;
+(UIColor *)activityType:(NSInteger)type;


-(UIColor *)changeAlpha:(CGFloat)alpha;
+(UIColor *)colorWith255Red:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
@end
