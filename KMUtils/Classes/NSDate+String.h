//
//  NSDate+String.h
//  Kmabas
//
//  Created by Mauricio Ventura on 11/10/16.
//  Copyright © 2016 Kmabas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (String)

-(NSString *)getHumanReadableStringWithFormat:(NSString *)format;
-(NSString *)getServerParamString;
-(NSString *)getYearString;
-(NSString *)getMonthName;
-(NSInteger)getMonthInteger;
-(NSString *)getMonthAndYear;
-(NSString *)getPropperDifference;
-(NSDate *)lastDayOfMonth;
+(NSDate *)dateFromString:(NSString *)dateStr withFormat:(NSString *)format;

@end
