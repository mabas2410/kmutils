//
//  UINavigationBar+AppBar.h
//  Kmabas
//
//  Created by Mauricio Ventura on 23/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (AppBar)
-(void)appBarWithLogo;
-(void)solidAppBar;
-(void)noAppBar;
-(void)colorAppBarWithAlpha:(CGFloat)alpha;
-(void)barWithTintColor:(UIColor *)color;
-(void)barWithColor:(UIColor *)color andTint:(UIColor *)tintColor;
@end
