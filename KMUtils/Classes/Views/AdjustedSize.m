//
//  AdjustedSize.m
//  Kmabas
//
//  Created by mabas on 22/09/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "AdjustedSize.h"

@implementation UILabel (AdjustSize)

-(CGFloat)getAdjustedheight
{
	UILabel *tmpLbl = [[UILabel alloc] initWithFrame:self.frame];
	[tmpLbl setText:self.text];
	[tmpLbl setFont:self.font];
	[tmpLbl setTextAlignment:self.textAlignment];
	[tmpLbl setNumberOfLines:0];
	[tmpLbl sizeToFit];
	return tmpLbl.frame.size.height;
}

@end
@implementation UITextView (AdjustSize)

-(CGFloat)getAdjustedheight
{
	
	UITextView *tmpText = [[UITextView alloc] initWithFrame:self.frame];
	[tmpText setText:self.text];
	[tmpText setFont:self.font];
	[tmpText setTextAlignment:self.textAlignment];
	CGSize newSize = [tmpText sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)];
	return newSize.height;
}

@end
@implementation UIImageView (AdjustSize)

-(CGSize)getAdjustedSize
{
	
	UIImage *image = [self image];
	if(!image)
		return CGSizeZero;
	
	CGFloat boundsWidth  = [self bounds].size.width;
	CGFloat boundsHeight = [self bounds].size.height;
	
	CGSize  imageSize  = [image size];
	CGFloat imageRatio = imageSize.width / imageSize.height;
	CGFloat viewRatio  = boundsWidth / boundsHeight;
	
	if(imageRatio < viewRatio) {
		CGFloat scale = boundsHeight / imageSize.height;
		CGFloat width = scale * imageSize.width;
		return CGSizeMake(width, boundsHeight);
	}
	
	CGFloat scale = boundsWidth / imageSize.width;
	CGFloat height = scale * imageSize.height;
	
	return CGSizeMake(boundsWidth, height);
}

@end
