//
//  UIView+LoadingAnimations.h
//  DEQ
//
//  Created by Mauricio Ventura on 08/11/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (LoadingAnimations)

-(void)startLoadingAnimation;
-(void)stopLoadingAnimation;
-(UIView *)rootView;
@end
