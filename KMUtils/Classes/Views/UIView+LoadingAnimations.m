//
//  UIView+LoadingAnimations.m
//  DEQ
//
//  Created by Mauricio Ventura on 08/11/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "UIView+LoadingAnimations.h"

@implementation UIView (LoadingAnimations)


-(void)startLoadingAnimation
{
	[self endEditing:true];
	UIView *shadow = [[UIView alloc] initWithFrame:self.bounds];
	shadow.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
	shadow.tag = -100;
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[shadow addSubview:spinner];
	spinner.center = shadow.center;
	[self addSubview:shadow];
	[spinner startAnimating];
	[self addSubview:shadow];
}
-(void)stopLoadingAnimation
{
	UIView *shadow = [self viewWithTag:-100];
	UIActivityIndicatorView *spinner = shadow.subviews.firstObject;
	
	[spinner stopAnimating];
	[shadow removeFromSuperview];
	shadow = nil;
	spinner = nil;
}
-(UIView *)rootView
{
	UIView *view = self;
	while (view.superview)
	{
		view = view.superview;
	}
	return view;
}
@end
