//
//  AdjustedSize.h
//  Kmabas
//
//  Created by mabas on 22/09/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (AdjustedSize)
-(CGFloat)getAdjustedheight;
@end

@interface UITextView (AdjustedSize)
-(CGFloat)getAdjustedheight;
@end

@interface UIImageView (AdjustedSize)
-(CGSize)getAdjustedSize;
@end
