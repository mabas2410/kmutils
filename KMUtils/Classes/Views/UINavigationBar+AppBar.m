//
//  UINavigationBar+AppBar.m
//  Kmabas
//
//  Created by Mauricio Ventura on 23/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "UINavigationBar+AppBar.h"
#import "UIFont+Fonts.h"
#import "UIColor+Pantone.h"
@implementation UINavigationBar (AppBar)
-(void)appBarWithLogo
{
	self.topItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-navbar"]];
	self.topItem.titleView.frame = CGRectMake(self.topItem.titleView.frame.origin.x, self.topItem.titleView.frame.origin.y + 18, self.topItem.titleView.frame.size.width, self.topItem.titleView.frame.size.height - 34);
	self.topItem.titleView.contentMode = UIViewContentModeScaleAspectFit;
}
-(void)solidAppBar
{
	self.shadowImage = [UIImage new];
	[self setBackgroundImage:[UIImage imageNamed:@"bg-navigation"] forBarMetrics:UIBarMetricsDefault];
	self.backgroundColor = [UIColor clearColor];
	self.tintColor = [UIColor whiteColor];
	self.barTintColor = [UIColor whiteColor];
	self.translucent = false;
	[self setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont controllerTitleFont]}];

}
-(void)noAppBar
{
	[self setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
	
	self.shadowImage = [UIImage new];
	self.backgroundColor = [UIColor clearColor];
	self.translucent = true;
	//[self setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor], NSFontAttributeName: [UIFont controllerTitleFont]}];

	[self barWithTintColor:[UIColor whiteColor]];
}

-(void)barWithColor:(UIColor *)color andTint:(UIColor *)tintColor
{
	UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
	if ([statusBar respondsToSelector:@selector(setBackgroundColor:)])
	{
		statusBar.backgroundColor = color;
	}
	[self setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
	self.shadowImage = [UIImage new];

	self.backgroundColor = color;

	self.translucent = true;
	[self barWithTintColor:tintColor];
}

-(void)colorAppBarWithAlpha:(CGFloat)alpha
{
	UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
	if ([statusBar respondsToSelector:@selector(setBackgroundColor:)])
	{
		statusBar.backgroundColor = [UIColor navBarSolidColorWithAlpha:alpha];
	}
	
	[self setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
	
	self.shadowImage = [UIImage new];
	self.backgroundColor = [UIColor navBarSolidColorWithAlpha:alpha];
	self.translucent = true;
	
	[self setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont controllerTitleFont]}];

}
-(void)barWithTintColor:(UIColor *)color
{
	self.tintColor = color;
	self.barTintColor = color;
	[self setTitleTextAttributes:@{NSForegroundColorAttributeName : color, NSFontAttributeName: [UIFont controllerTitleFont]}];
}
@end
