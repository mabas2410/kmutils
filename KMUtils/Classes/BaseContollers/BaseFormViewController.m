//
//  BaseFormViewController.m
//  Kmabas
//
//  Created by Mauricio Ventura on 24/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "BaseFormViewController.h"

@interface BaseFormViewController ()

@end

@implementation BaseFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyboard) name:UIKeyboardWillHideNotification object:nil];
}

-(void)showKeyboard
{
	[self.view layoutIfNeeded];
	[UIView animateWithDuration:0.5 animations:^{
		self.bottomSpaceConstraint.constant = 260;
		[self.view layoutIfNeeded];
	}];
}
-(void)hideKeyboard
{
	[self.view layoutIfNeeded];
	[UIView animateWithDuration:0.5 animations:^{
		self.bottomSpaceConstraint.constant = 0;
		[self.view layoutIfNeeded];
	}];

}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	[self propagateTouchesEnded:touches withEvent:event toView:self.view];
}
-(void)propagateTouchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event toView:(UIView *)view
{
	if ([view isKindOfClass:[FormInputView class]])
	{
		[view touchesEnded:touches withEvent:event];
	}
	else
	{
		for (UIView *subview in [view subviews])
		{
			[self propagateTouchesEnded:touches withEvent:event toView:subview];
		}
	}
	
}

@end
