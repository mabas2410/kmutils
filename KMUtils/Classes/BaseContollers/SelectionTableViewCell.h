//
//  SelectionTableViewCell.h
//  Health Test
//
//  Created by Mauricio Ventura on 15/11/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *accesoryView;

@end
