//
//  SelectionTableViewCell.m
//  Health Test
//
//  Created by Mauricio Ventura on 15/11/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import "SelectionTableViewCell.h"

@implementation SelectionTableViewCell

- (void)awakeFromNib {
	[super awakeFromNib];
	// Initialization code
}

- (void)setSelected:(BOOL)selected
{
	self.accesoryView.layer.cornerRadius = self.accesoryView.frame.size.width / 2;
	self.accesoryView.layer.borderWidth = 2;
	self.accesoryView.layer.borderColor = self.tintColor.CGColor;
	if (selected) {
		self.contentView.alpha = 1;
		
		self.accesoryView.backgroundColor = self.tintColor;
		
	}
	else
	{
		self.contentView.alpha = 0.5;
		self.accesoryView.backgroundColor = [UIColor clearColor];
		
		
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	self.accesoryView.layer.cornerRadius = self.accesoryView.frame.size.width / 2;
	self.accesoryView.layer.borderWidth = 2;
	self.accesoryView.layer.borderColor = self.tintColor.CGColor;
	if (selected) {
		self.contentView.alpha = 1;
		
		self.accesoryView.backgroundColor = self.tintColor;
		
	}
	else
	{
		self.contentView.alpha = 0.5;
		self.accesoryView.backgroundColor = [UIColor clearColor];
		
		
	}
}

@end

