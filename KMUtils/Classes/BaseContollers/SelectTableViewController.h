//
//  SelectTableViewController.h
//  Health Test
//
//  Created by Mauricio Ventura on 15/11/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import <KMUtils/BaseViewController.h>
@class SelectTableViewController;
@protocol SelectTableViewControllerDelegate<NSObject>
@required
-(void)selectTableViewController:(SelectTableViewController *)selectTVC didFinishSelecting:(NSArray *)selected;

@end
@interface SelectTableViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>

typedef NS_ENUM(NSInteger, Selection)
{
	SelectionOptional,
	SelectionOne,
	SelectionRange,
};
@property (nonatomic, assign) Selection selectionType;
@property (nonatomic, assign) CGPoint selectionMinMax;
@property (weak, nonatomic)id <SelectTableViewControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *selectables;
@property (strong, nonatomic) NSMutableArray *selected;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nextBtn;


-(void)manageNextButton;
- (IBAction)next:(id)sender;
-(void)reloadData;
@end
