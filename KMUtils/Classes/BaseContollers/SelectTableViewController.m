//
//  SelectTableViewController.m
//  Health Test
//
//  Created by Mauricio Ventura on 15/11/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import "SelectTableViewController.h"
#import "SelectionTableViewCell.h"
@interface SelectTableViewController ()

@end

@implementation SelectTableViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self manageNextButton];
	self.selected = [NSMutableArray array];
	UINib *nib = [UINib nibWithNibName:@"SelectionTableViewCell" bundle:nil];
	[[self tableView] registerNib:nib forCellReuseIdentifier:@"selectableCell"];

}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.selectables.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	SelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"selectableCell"];
	if (!cell)
	{
		cell = [[SelectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"selectableCell"];
	}
	cell.textLabel.text = [NSString stringWithFormat:@"opción %ld", (long)indexPath.row];
	[self manageNextButton];
	return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.nextBtn.enabled = true;
	if (self.tableView.allowsMultipleSelection)
	{
		[self.selected addObject:[self.selectables objectAtIndex:indexPath.row]];
	}
	else
	{
		self.selected = [@[[self.selectables objectAtIndex:indexPath.row]] mutableCopy];
	}
	[self manageNextButton];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self.selected removeObject:[self.selectables objectAtIndex:indexPath.row]];
}
-(void)manageNextButton
{
	switch (self.selectionType)
	{
		case SelectionOptional:
			self.nextBtn.enabled = true;
			break;
		case SelectionOne:
			if (self.selected.count == 1)
			{
				self.nextBtn.enabled = true;
			}
			else
			{
				self.nextBtn.enabled = false;
			}
			break;
		case SelectionRange:
			if (self.selected.count >= self.selectionMinMax.x && self.selected.count <= self.selectionMinMax.y)
			{
				self.nextBtn.enabled = true;
			}
			else
			{
				self.nextBtn.enabled = false;
			}
			break;
		default:
			break;
	}
}

-(void)next:(id)sender
{
	[self.delegate selectTableViewController:self didFinishSelecting:self.selected];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 80;
}
-(void)reloadData
{
	[self.tableView reloadData];
}
@end
