//
//  BaseFormViewController.h
//  Kmabas
//
//  Created by Mauricio Ventura on 24/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectableFormInputView.h"

#import "BaseViewController.h"
#import "AppNavigationViewController.h"
@interface BaseFormViewController : BaseViewController<FormInputViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceConstraint;

@end
