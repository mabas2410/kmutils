//
//  AppNavigationViewController.m
//  Kmabas
//
//  Created by Mauricio Ventura on 23/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "AppNavigationViewController.h"
@interface AppNavigationViewController ()

@end

@implementation AppNavigationViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self preferredStatusBarStyle];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
	return [self.viewControllers.lastObject preferredStatusBarStyle];
}





@end
