//
//  BaseViewController.h
//  Kmabas
//
//  Created by Mauricio Ventura on 30/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppNavigationViewController.h"

@interface BaseViewController : UIViewController

@end
