//
//  AppNavigationViewController.h
//  Kmabas
//
//  Created by Mauricio Ventura on 23/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationBar+AppBar.h"
@interface AppNavigationViewController : UINavigationController
@end
