//
//  TutorialView.m
//  Kmabas
//
//  Created by Mauricio Ventura on 19/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "TutorialView.h"

@implementation TutorialView
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
		self.view = [[[NSBundle mainBundle] loadNibNamed:@"TutorialC" owner:self options:nil] firstObject];

		[self addSubview:self.view];
	}
	return self;
}
-(instancetype)initWithFrame:(CGRect)frame andXibName:(NSString *)xibName
{
	if (self = [super initWithFrame:frame])
	{
		self.view = [[[NSBundle mainBundle] loadNibNamed:xibName owner:self options:nil] firstObject];
		self.view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
		[self addSubview:self.view];
	}
	return self;
}
-(void)layoutIfNeeded
{
	[super layoutIfNeeded];
}

@end
