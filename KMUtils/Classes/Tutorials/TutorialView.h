//
//  TutorialView.h
//  Kmabas
//
//  Created by Mauricio Ventura on 19/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialView : UIView
@property (nonatomic, weak) IBOutlet UIView *view;
-(instancetype)initWithFrame:(CGRect)frame andXibName:(NSString *)xibName;
@end
