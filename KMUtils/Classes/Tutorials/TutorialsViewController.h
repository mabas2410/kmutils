//
//  TutorialsViewController.h
//  Kmabas
//
//  Created by Mauricio Ventura on 19/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialsViewController : UIViewController<UIScrollViewDelegate>

@property (strong) NSArray *xibNames;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollWidthConstraint;
- (IBAction)pageControl:(UIPageControl *)sender;

@end
