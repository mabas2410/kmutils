//
//  TutorialsViewController.m
//  Kmabas
//
//  Created by Mauricio Ventura on 19/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "TutorialsViewController.h"
#import "TutorialView.h"
@interface TutorialsViewController ()

@end

@implementation TutorialsViewController
{
	__weak IBOutlet UIView *contentView;
	NSMutableArray *views;
}
- (void)viewDidLoad
{
	[super viewDidLoad];
	self.view.backgroundColor = [UIColor clearColor];
	self.scrollView.backgroundColor = [UIColor clearColor];
	
	
	
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	
	int viewCount = 0;
	if (contentView.subviews.count == 0)
	{
		for (NSString *xibName in self.xibNames)
		{
			TutorialView *cView = [[TutorialView alloc] initWithFrame:CGRectMake((self.view.frame.size.width * viewCount), 0, self.view.frame.size.width, self.view.frame.size.height - 30) andXibName:xibName];
			if (cView)
			{
				[contentView addSubview:cView];
				viewCount++;
			}
		}
		self.scrollWidthConstraint.constant = self.xibNames.count * self.view.frame.size.width;
		[self.view layoutIfNeeded];
	}
	
	[self.view layoutIfNeeded];
}
-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	

}
- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
 NSInteger pageNumber = roundf(scrollView.contentOffset.x / (scrollView.frame.size.width));
	self.pageControl.currentPage = pageNumber;
}
- (IBAction)pageControl:(UIPageControl *)sender
{
	CGFloat x = sender.currentPage * self.scrollView.frame.size.width;
	[self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}
@end
