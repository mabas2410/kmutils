//
//  SemiModalPickerViewController.h
//  DEQ
//
//  Created by Mauricio Ventura on 15/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SemiModalViewController.h"

@class PickerSemiModalViewController;
@protocol PickerSemiModalViewControllerDelegate<NSObject>


@required
-(void)pickerSemiModalViewController:(PickerSemiModalViewController *)picker didSelectStructure:(NSArray *)structure; //tag:(NSInteger)tag;
-(void)pickerSemiModalViewControllerDidCancel:(PickerSemiModalViewController *)picker;

@optional

@end
@protocol PickerSemiModalViewControllerDataSource<NSObject>


@required
-(void)numberOfComponentsInPickerSemiModalView:(PickerSemiModalViewController *)semimodalViewController; //tag:(NSInteger)tag;
-(void)pickerSemiModalViewController:(PickerSemiModalViewController *)semimodalViewController numberOfRowsInComponent:(NSInteger)component;


@optional

@end

@interface PickerSemiModalViewController : SemiModalViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, weak) id <PickerSemiModalViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerView;


@property (strong) NSArray *pickerStructure;

- (IBAction)select:(id)sender;
- (IBAction)cancel:(id)sender;

@end
