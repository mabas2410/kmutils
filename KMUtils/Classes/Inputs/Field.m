//
//  Field.m
//  DEQ
//
//  Created by Mauricio Ventura on 06/01/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import "Field.h"
#import "NSDate+String.h"

/*
NSString * const kNone = @"";

NSString * const kAllowCero = @"allowCero";
NSString * const kBoolean = @"allowBoolean";
NSString * const kAllowNegative = @"allowNegative";
NSString * const kAllowEmpty = @"allowEmpty";
NSString * const kAllowNil = @"allowNil";

NSString * const kNotToday = @"notToday";
NSString * const kNotPast = @"notPast";
NSString * const kNotFuture = @"notFuture";
*/

NSString * const kFieldSuccessKey = @"success";
NSString * const kFieldMessageKey = @"message";
NSString * const kFieldErrorsKey = @"errors";


NSString * const kFieldKeyKey = @"key";
NSString * const kFieldTypeKey = @"type";
NSString * const kFieldLabelKey = @"label";
NSString * const kFieldPlaceholderKey = @"placeholder";
NSString * const kFieldClassNameKey = @"className";
NSString * const kFieldOptionsKey = @"options";
NSString * const kFieldSelectOptionsKey = @"selectOptions";



@implementation Field
{
	NSInteger greaterThan;
	NSInteger equalTo;
	NSInteger lessThan;
	NSString *compareString;
}
-(instancetype)initWithDict:(NSDictionary *)dict
{
	if (self = [super init])
	{
		self.paramKey = [dict objectForKey:kFieldKeyKey];
		self.type = [[dict objectForKey:kFieldTypeKey] integerValue];
		self.label = [dict objectForKey:kFieldLabelKey];
		self.placeholder = [dict objectForKey:kFieldPlaceholderKey];
		self.stringValue = @"";
		
		self.className = [dict objectForKey:kFieldClassNameKey];
		self.validationOptions = [[dict objectForKey:kFieldOptionsKey] mutableCopy];
		
		if ([[dict objectForKey:@"selectOpitons"] isKindOfClass:[NSArray class]])
		{
			self.selectOptions = [dict objectForKey:@"selectOpitons"];
		}
		else if ([[dict objectForKey:@"selectOpitons"] isEqualToString:@"date"])
		{
			
		}
	}
	return self;
}

- (id)validateAndGetError:(NSError **)error
{
	NSArray *errors = [self testField];
	if (errors.count > 0)
	{
		*error = [NSError errorWithDomain:@"com.field.validation" code:1 userInfo:@
						 {
						 NSLocalizedDescriptionKey: [errors.firstObject objectForKey:kFieldMessageKey],
						 NSLocalizedRecoverySuggestionErrorKey: self.label
						 }];
	}
	return self.fieldValue;
}

-(NSDictionary *)validateField
{
	NSMutableDictionary *dictionary = [@
	{
		kFieldSuccessKey: @(true),
	} mutableCopy];
	NSArray *errors = [self testField];
	if (errors.count > 0)
	{
		[dictionary setObject:@(false) forKey:kFieldMessageKey];
		[dictionary setObject:[errors.firstObject objectForKey:kFieldMessageKey] forKey:kFieldMessageKey];
		[dictionary setObject:errors forKey:kFieldErrorsKey];
		[dictionary setObject:@(false) forKey:kFieldSuccessKey];
	}
	self.validationResult = dictionary;
	return self.validationResult;

}
-(NSArray *)testField
{
	NSMutableArray *errors = [NSMutableArray array];

	id value = [self getValueFromString:self.stringValue];

	for (id option in self.validationOptions)
	{
		if ([option isKindOfClass:[NSNumber class]])
		{
			NSDictionary *error = [self validateOption:(ValidationOption)[option integerValue] inValue:value];
			if (error)
			{
				[errors addObject:error];
			}
		}
		else if ([option isKindOfClass:[NSDictionary class]])
		{
			for (NSNumber *optionKey in [option allKeys])
			{
				NSInteger validationOption = (ValidationOption)[optionKey integerValue];
				switch (validationOption)
				{
					case ValidationOptionLengthEqualTo:
						equalTo = [[option objectForKey:optionKey] integerValue];
						break;
					case ValidationOptionLengthGreaterThan:
						greaterThan = [[option objectForKey:optionKey] integerValue];
						break;
					case ValidationOptionLengthLessThan:
						lessThan = [[option objectForKey:optionKey] integerValue];
						break;
					case ValidationOptionEqualToString:
					case ValidationOptionNotEqualToString:
						compareString = [option objectForKey:optionKey];
						break;
					
					default:
						break;
				}
				NSDictionary *error = [self validateOption:validationOption inValue:value];
				if (error)
				{
					[errors addObject:error];
				}
			}
			
		}
		
	}
	if (errors.count == 0)
	{
		self.fieldValue = value;
	}
	return errors;
}
-(id)getValueFromString:(NSString *)string
{
	switch (self.type)
	{
		case FieldTypeString: case FieldTypeEmail: case FieldTypeURL: case FieldTypePhoneNumber:
			return string;
			break;
		case FieldTypeNumber:
			return [NSNumber numberWithInteger:[string integerValue]];
			break;
		case FieldTypeFloat:
			return [NSNumber numberWithInteger:[string floatValue]];
			break;
		case FieldTypeDate:
			return [NSDate dateFromString:string withFormat:@"yyyy-MM-dd"];
			break;
		case FieldTypeSelectable:
			return string;
			break;
  default:
			return string;

			break;
	}
	return nil;
}

-(NSDictionary *)validateOption:(ValidationOption)option inValue:(id)value
{
	switch (option)
	{
		case ValidationOptionNotNil:
		{
			if (!value) {
				return @{kFieldMessageKey: NSLocalizedString(@"notNilAllowed", nil)};
			}
			return nil;
		}
			break;
		case ValidationOptionNumber: ValidationOptionFloatNumber:
		{
			if (![value isKindOfClass:[NSNumber class]])
			{
				return @{kFieldMessageKey: NSLocalizedString(@"mustBeFloat", nil)};
			}
			return nil;
		}
			break;
		case ValidationOptionNaturalNumber:
		{
			if ([value isKindOfClass:[NSNumber class]])
			{
				if([value integerValue] < 0)
				{
					return @{kFieldMessageKey: NSLocalizedString(@"notNegativeAllowed", nil)};
				}
			}
			return nil;
		}
			break;
		
		case ValidationOptionNotEmptyString:
		{
			if ([value isKindOfClass:[NSString class]])
			{
				if([(NSString *)value length] < 1)
				{
					return @{kFieldMessageKey: NSLocalizedString(@"notEmptyStringAllowed", nil)};
				}
			}
			else
			{
				return @{kFieldMessageKey: NSLocalizedString(@"notEmptyStringAllowed", nil)};
			}
			return nil;
		}
			break;
		case ValidationOptionLengthEqualTo:
		{
			
			if ([self.stringValue length] != equalTo)
			{
				return @{kFieldMessageKey: [NSString stringWithFormat:@"%@: %ld", NSLocalizedString(@"lengthShouldBeEqualTo", nil), (long)equalTo]};
			}
		}
			break;
		case ValidationOptionLengthLessThan:
		{
			if ([self.stringValue length] >= lessThan)
			{
				return @{kFieldMessageKey: [NSString stringWithFormat:@"%@: %ld", NSLocalizedString(@"lengthShouldBeLessThan", nil), (long)lessThan]};
			}
		}
			break;
		case ValidationOptionLengthGreaterThan:
		{
			if ([self.stringValue length] <= greaterThan)
			{
				return @{kFieldMessageKey: [NSString stringWithFormat:@"%@: %ld", NSLocalizedString(@"lengthShouldBeGreaterThan", nil), (long)greaterThan]};
			}
		}
			break;
		case ValidationOptionEqualToString:
		{
			if ([self.stringValue isEqualToString:compareString])
			{
				return @{kFieldMessageKey: [NSString stringWithFormat:@"%@", NSLocalizedString(@"passwordMismatch", nil)]};
			}
		}
		case ValidationOptionMustEmail:
		{
			if ([value isKindOfClass:[NSString class]])
			{
				if(![self isEmail])
				{
					return @{kFieldMessageKey: NSLocalizedString(@"mustBeEmail", nil)};
				}
			}
			else
			{
				return @{kFieldMessageKey: NSLocalizedString(@"mustBeEmail", nil)};
			}
			return nil;
		}
			break;
		case ValidationOptionMustURL:
		{
			if ([value isKindOfClass:[NSString class]])
			{
				if(![(NSString *)value containsString:@"."])
				{
					return @{kFieldMessageKey: NSLocalizedString(@"mustBeURL", nil)};
				}
			}
			else
			{
				return @{kFieldMessageKey: NSLocalizedString(@"mustBeURL", nil)};
			}
			return nil;
		}
			break;
		case ValidationOptionPast:
		{
			if ([value isKindOfClass:[NSDate class]])
			{
				if([[NSDate dateFromString:[[NSDate date] getHumanReadableStringWithFormat:@"yyyy-MM-dd"] withFormat:@"yyyy-MM-dd"] timeIntervalSinceDate:(NSDate *)value] <= 0)
				{
					return @{kFieldMessageKey: NSLocalizedString(@"mustBePast", nil)};
				}
			}
			else
			{
				return @{kFieldMessageKey: NSLocalizedString(@"mustBePast", nil)};
			}
		}
			break;
		case ValidationOptionFuture:
		{
			
		}
			break;
		default:
			return nil;
			break;
	}
	return nil;
}
- (BOOL)isEmail
{
	NSString *emailRegex = @"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	BOOL isValid = [emailTest evaluateWithObject:self.stringValue];
	return isValid;
}


-(NSDictionary *)getDictionary
{
	return @
	{
		kFieldTypeKey: @(self.type),
		kFieldLabelKey: self.label,
		kFieldPlaceholderKey: self.placeholder,
		kFieldKeyKey:self.paramKey,
		kFieldClassNameKey: self.className,
		kFieldOptionsKey: self.validationOptions,
		kFieldSelectOptionsKey: self.selectOptions
	};
}
@end
