//
//  FormInputView.h
//  Kmabas
//
//  Created by Mauricio Ventura on 21/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Field.h"

@class FormInputView;
@protocol FormInputViewDelegate<NSObject>

@optional
-(void)formInputViewDidStartEditing:(FormInputView *)formInput;
-(void)formInputViewDidEndEditing:(FormInputView *)formInput;
-(void)formInputView:(FormInputView *)formInput accesoryButtonDidPressed:(UIButton *)sender;

@end
IB_DESIGNABLE

@interface FormInputView : UIView<UITextFieldDelegate>

@property (nonatomic) IBInspectable NSString *labelName;
@property (nonatomic) IBInspectable NSString *inputImage;
@property(nonatomic) IBInspectable NSInteger inputImageViewWidth;

@property (nonatomic) IBInspectable UIImage *accesoryImage;
@property(nonatomic) IBInspectable BOOL secureText;


	//ValidationKeys

@property (nonatomic, strong) NSDictionary *validationResult;
@property (weak, nonatomic) IBOutlet UIImageView *inputImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputImageViewWidthConstraint;


@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *inputLabel;
@property (weak, nonatomic) IBOutlet UILabel *errorLbl;
@property (weak, nonatomic) IBOutlet UIImageView *errorImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblSeparationConstraint;
@property (weak, nonatomic) IBOutlet UIButton *accesoryButton;

@property (weak, nonatomic) IBOutlet id<FormInputViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet FormInputView *nextInput;

@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (strong, nonatomic) Field *field;


-(void)endEditing;
-(void)startEditing;
- (IBAction)editField:(id)sender;

- (IBAction)executeAccesoryAction:(UIButton *)sender;
-(NSDictionary *)validate;
-(void)showErrorWithText:(NSString *)text;
-(void)hideError;

+(void)validateInputViews:(NSArray *)inputViews error:(NSError **)error;
@end
