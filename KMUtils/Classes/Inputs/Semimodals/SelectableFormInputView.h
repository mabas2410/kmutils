//
//  SelectableFormInputView.h
//  Kmabas
//
//  Created by Mauricio Ventura on 28/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "FormInputView.h"
#import "PickerSemiModalViewController.h"
@class SelectableFormInputView;
@protocol SelectableFormInputViewTransitionDelegate<NSObject>
@required
-(void)formInputView:(SelectableFormInputView *)formInput shouldPresentPicker:(PickerSemiModalViewController *)semimodalPicker;
-(void)formInputView:(SelectableFormInputView *)formInput shouldDismissPicker:(PickerSemiModalViewController *)picker;

@end
@interface SelectableFormInputView : FormInputView<PickerSemiModalViewControllerDelegate>
@property (strong)NSString *joinString;
@property (weak, nonatomic) id<SelectableFormInputViewTransitionDelegate> transitionDelegate;
@property (weak, nonatomic) PickerSemiModalViewController *semiModal;
@end
