//
//  SemiModalViewController.m
//  DEQ
//
//  Created by Mauricio Ventura on 21/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "SemiModalViewController.h"

@interface SemiModalViewController ()

@end

@implementation SemiModalViewController

- (void)viewDidLoad {
	[super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}
-(void)present
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[UIView animateWithDuration:0.4 delay:0.6 options:UIViewAnimationOptionCurveEaseIn animations:^{
			self.cancelView.alpha = 1;
		} completion:nil];

	});
}
-(void)dismissWithCompletion:(void (^)(void))completion
{
	[UIView animateWithDuration:0.4 animations:^{
		self.cancelView.alpha = 0;
	} completion:^(BOOL finished)
	 {
		 dispatch_async(dispatch_get_main_queue(), ^{
			 [self.view.superview layoutIfNeeded];
			 [UIView animateWithDuration:0.7 animations:^{
				 [self.view.superview layoutIfNeeded];
				 self.view.center = CGPointMake(self.view.center.x, self.view.center.y * 3);
				 self.view.alpha = 0;
			 } completion:^(BOOL finished)
				{
					[self removeFromParentViewController];
					completion();
				}];
		 });
		 
	 }];
}


@end
