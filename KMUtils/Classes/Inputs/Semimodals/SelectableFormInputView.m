//
//  SelectableFormInputView.m
//  Kmabas
//
//  Created by Mauricio Ventura on 28/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "SelectableFormInputView.h"
#import "NSDate+String.h"
@implementation SelectableFormInputView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setField:(Field *)field
{
	super.field = field;
	
}
-(void)editField:(id)sender
{
	[self startEditing];
}
-(void)startEditing
{
	NSLog(@"startEditing");
	[self.superview endEditing:true];
	[self.superview layoutIfNeeded];
	[UIView animateWithDuration:0.3 animations:^{
		self.lblSeparationConstraint.constant = 0;
		self.textField.userInteractionEnabled = false;
		dispatch_async(dispatch_get_main_queue(), ^{
			if ([self.transitionDelegate respondsToSelector:@selector(formInputView:shouldPresentPicker:)])
			{
				NSString *identifier = @"pickerSemimodal";
				if (self.field.type == FieldTypeDate)
				{
					identifier = @"datePickerSemimodal";
				}
				self.semiModal = [[UIStoryboard storyboardWithName:@"KMUtils" bundle:nil] instantiateViewControllerWithIdentifier:identifier];
				self.semiModal.delegate = self;
				self.semiModal.tag = self.tag;
				if (self.field.type == FieldTypeSelectable) {
					self.semiModal.pickerStructure = self.field.selectOptions;
					[self.semiModal.pickerView reloadAllComponents];
				}
				else if (self.field.type == FieldTypeDate)
				{
					self.semiModal.pickerStructure = @[];
				}
				[self.transitionDelegate formInputView:self shouldPresentPicker:self.semiModal];
			}
		});
		[self.superview layoutIfNeeded];

	} completion:^(BOOL finished)
	{
		
	}];
	
}
-(void)endEditing
{
	NSLog(@"endEditing");
	
	[self layoutIfNeeded];
	//[self validate];
	[UIView animateWithDuration:0.2 animations:^{
		if (self.textField.text.length == 0)
		{
			self.lblSeparationConstraint.constant = self.textField.frame.size.height * -1;
		}
		[self layoutIfNeeded];
	} completion:^(BOOL finished) {
		[self layoutIfNeeded];
		dispatch_async(dispatch_get_main_queue(), ^{
			if ([self.delegate respondsToSelector:@selector(formInputViewDidEndEditing:)])
			{
				[self.delegate formInputViewDidEndEditing:self];
			}
		});
		
	}];
	
}
-(void)pickerSemiModalViewController:(PickerSemiModalViewController *)picker didSelectStructure:(NSArray *)structure
{
	NSString *structureJoined = @"";
	
	if (self.field.type == FieldTypeDate)
	{
		if (!self.joinString)
		{
			self.joinString = @"yyyy-MM-dd";
		}
		structureJoined = [(NSDate *)structure.firstObject getHumanReadableStringWithFormat:self.joinString];
	}
	else
	{
		if (!self.joinString)
		{
			self.joinString = @"";
		}
		int selectOptionsIndex = 0;
		for (NSNumber *valueIndex in structure)
		{
			if (structureJoined.length > 0)
			{
				structureJoined = [NSString stringWithFormat:@"%@%@%@", structureJoined, self.joinString, [[self.field.selectOptions objectAtIndex:selectOptionsIndex] objectAtIndex:[valueIndex integerValue]]];
			}
			else
			{
				structureJoined = [NSString stringWithFormat:@"%@", [[self.field.selectOptions objectAtIndex:selectOptionsIndex] objectAtIndex:[valueIndex integerValue]]];
				
			}
		}

	}
	self.textField.text = structureJoined;
	[self endEditing];

	[picker dismissWithCompletion:^{
	}];}
-(void)pickerSemiModalViewControllerDidCancel:(PickerSemiModalViewController *)picker
{
	[self endEditing];
	[picker dismissWithCompletion:^{
	}];
}
@end
