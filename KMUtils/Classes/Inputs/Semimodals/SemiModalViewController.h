//
//  SemiModalViewController.h
//  DEQ
//
//  Created by Mauricio Ventura on 21/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SemiModalViewController : UIViewController

@property (nonatomic, assign) NSInteger tag;

@property (weak, nonatomic) IBOutlet UIButton *cancelView;
@property (weak, nonatomic) IBOutlet UIButton *ok;

-(void)present;
-(void)dismissWithCompletion:(void (^)(void))completion;
@end
