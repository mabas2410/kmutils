//
//  Field.h
//  DEQ
//
//  Created by Mauricio Ventura on 06/01/17.
//  Copyright © 2017 ezapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FieldValidationResult;
typedef NS_ENUM(NSInteger, FieldType)
{
	FieldTypeString,
	FieldTypeEmail,
	FieldTypeURL,
	
	FieldTypeCard,
	FieldTypeExpCard,

	FieldTypeNumber,
	FieldTypePhoneNumber,
	FieldTypeZipCode,
	FieldTypeFloat,
	FieldTypeButtonOptions,
	FieldTypeSelectable,
	FieldTypeDate
};


typedef NS_ENUM(NSInteger, ValidationOption)
{
	ValidationOptionNone,
	ValidationOptionNotNil,
	ValidationOptionNotEmptyString,

	ValidationOptionMustURL,
	ValidationOptionMustEmail,
	
	ValidationOptionLengthGreaterThan,
	ValidationOptionLengthLessThan,
	ValidationOptionLengthEqualTo,
	ValidationOptionEqualToString,
	ValidationOptionNotEqualToString,
	
	ValidationOptionNumber,
	ValidationOptionPhoneNumber,
	ValidationOptionNaturalNumber,
	ValidationOptionFloatNumber,
	
	ValidationOptionFuture,
	ValidationOptionPast,
	ValidationOptionNotToday
};

/*
typedef enum
{
	None = 0,
	NotDefined, //1
	NotZero, //2
	MustBool, //3
	NotNegative, //4
	NotEmptyString, //5
	NotNil, //6
	NotToday, //7
	NotPast, //8
	NotFuture, //9
	ClassNotMatch, //10
	MustUrl, //11
	MustEmail //12
} ValidationOptions;
*/


/*
extern NSString * const kNone;

extern NSString * const kAllowCero;
extern NSString * const kBoolean;
extern NSString * const kAllowNegative;
extern NSString * const kAllowEmpty;
extern NSString * const kAllowNil;

extern NSString * const kNotToday;
extern NSString * const kNotPast;
extern NSString * const kNotFuture;
 */
extern NSString * const kFieldSuccessKey;
extern NSString * const kFieldMessageKey;
extern NSString * const kFieldErrorsKey;



extern NSString * const kFieldKeyKey;
extern NSString * const kFieldTypeKey;
extern NSString * const kFieldLabelKey;
extern NSString * const kFieldPlaceholderKey;
extern NSString * const kFieldClassNameKey;
extern NSString * const kFieldOptionsKey;
extern NSString * const kFieldSelectOptionsKey;


@interface Field : NSObject

@property (strong) NSString *label;
@property (strong) NSString *placeholder;
@property (strong) NSString *paramKey;
@property (nonatomic, assign) FieldType type;

@property (strong) NSArray *selectOptions;

@property (strong) NSString *className;
@property (strong) NSMutableArray *validationOptions;
@property (strong) NSMutableDictionary *validationResult;

@property (strong) id fieldValue;
@property (strong) NSString *stringValue;

-(instancetype)initWithDict:(NSDictionary *)dict;
- (id)validateAndGetError:(NSError **)error;
-(NSDictionary *)validateField;
-(NSArray *)testField;
-(NSDictionary* )validateOption:(ValidationOption)option inValue:(id)value;
-(NSDictionary *)getDictionary;


@end

