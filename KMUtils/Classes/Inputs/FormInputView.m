//
//  FormInputView.m
//  Kmabas
//
//  Created by Mauricio Ventura on 21/08/17.
//  Copyright © 2017 Kmabas. All rights reserved.
//

#import "FormInputView.h"
#import "HBPhoneNumberFormatter.h"
#import <KMUtils/SelectableFormInputView.h>

@implementation FormInputView
{
	BOOL shouldGoToNextInput;
	NSString *tmpPass;
	HBPhoneNumberFormatter *formatter;
	BOOL added;
	
}
- (instancetype)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame])
	{
		[[UINib nibWithNibName:@"FormInputView" bundle:[NSBundle bundleForClass:[self class]]] instantiateWithOwner:self options:nil];
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		[self addSubview:self.contentView];
		[self customInit];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
		[[UINib nibWithNibName:@"FormInputView" bundle:[NSBundle bundleForClass:[self class]]] instantiateWithOwner:self options:nil];
		self.contentView.frame = self.bounds;
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		
		[self addSubview:self.contentView];
		
		[self customInit];
		
	}
	return self;
}

-(void)prepareForInterfaceBuilder
{
	[self customInit];
}
- (void)drawRect:(CGRect)rect
{
	[self customInit];
}

- (void)setNeedsLayout
{
	[super setNeedsLayout];
	[self setNeedsDisplay];
}
- (void)customInit
{
	if (!self.inputImage)
	{
		self.inputImageViewWidthConstraint.constant = self.inputImageViewWidth;
		
		
	}
	else
	{
		self.inputImageView.image = [UIImage imageNamed:self.inputImage];
		self.inputImageViewWidthConstraint.constant = 30;
	}
	
	self.backgroundColor = [UIColor clearColor];
	
	[self.inputLabel setTitle:self.labelName forState:UIControlStateNormal];
	if (self.accesoryImage)
	{
		[self.accesoryButton setBackgroundImage:self.accesoryImage forState:UIControlStateNormal];
		self.accesoryButton.hidden = false;
	}
	if (!self.nextInput) {
		self.textField.returnKeyType = UIReturnKeyDone;
	}
	switch (self.field.type)
	{
		case FieldTypeNumber:
			self.textField.keyboardType = UIKeyboardTypeNumberPad;
			break;
		case FieldTypePhoneNumber:
			self.textField.keyboardType = UIKeyboardTypeNumberPad;
			formatter = [[HBPhoneNumberFormatter alloc] initWithFormatting:@" (55) 1111-1111"];
			break;
		case FieldTypeZipCode:
			self.textField.keyboardType = UIKeyboardTypeNumberPad;
			formatter = [[HBPhoneNumberFormatter alloc] initWithFormatting:@"11111"];
			break;
		case FieldTypeCard:
			self.textField.keyboardType = UIKeyboardTypeNumberPad;
			formatter = [[HBPhoneNumberFormatter alloc] initWithFormatting:@"1111 1111 1111 1111"];
			break;
		case FieldTypeExpCard:
			self.textField.keyboardType = UIKeyboardTypeNumberPad;
			formatter = [[HBPhoneNumberFormatter alloc] initWithFormatting:@"11 / 11"];
			break;
		case FieldTypeURL:
			self.textField.keyboardType = UIKeyboardTypeURL;
			break;
		case FieldTypeEmail:
			self.textField.keyboardType = UIKeyboardTypeEmailAddress;
			self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
			self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
			break;
		case FieldTypeFloat:
			self.textField.keyboardType = UIKeyboardTypeDecimalPad;
			break;
		case FieldTypeString:
			self.textField.keyboardType = UIKeyboardTypeDefault;
			break;
			
		default:
			self.textField.keyboardType = UIKeyboardTypeDefault;
			
			break;
	}
	self.textField.secureTextEntry = self.secureText;
	self.field.label = self.labelName;
	
}
-(void)setupAccesoryButtonWithImage:(UIImage *)image
{
	[self.accesoryButton setImage:image forState:UIControlStateNormal];
	self.accesoryButton.hidden = false;
}
/*-(void)setInputImage:(UIImage *)inputImage
 {
 self.inputImageView.image = inputImage;
 }
 */
- (IBAction)editField:(UIButton *)sender
{
	if (self.lblSeparationConstraint.constant < 0 || !self.textField.isFirstResponder)
	{
		
		[self startEditing];
	}
	else if (self.textField.text.length > 0)
	{
	}
	else
	{
		NSError *error;
		[self.field validateAndGetError:&error];
		if (!error)
		{
			NSLog(@"Message of error: %@", error.localizedDescription);
		}
		[self.textField resignFirstResponder];
	}
	
}

- (IBAction)executeAccesoryAction:(UIButton *)sender
{
	[self.textField resignFirstResponder];
	if ([self.delegate respondsToSelector:@selector(formInputView:accesoryButtonDidPressed:)])
	{
		[self.delegate formInputView:self accesoryButtonDidPressed:sender];
	}
}
-(void)setField:(Field *)field
{
	_field = field;
	
}
-(void)startEditing
{
	NSLog(@"startEditing");
	[self.contentView bringSubviewToFront:self.textField];
	[self.contentView bringSubviewToFront:self.accesoryButton];
	switch (self.field.type)
	{
		case FieldTypeNumber:
			[self.textField becomeFirstResponder];
			break;
		case FieldTypePhoneNumber:
			[self.textField becomeFirstResponder];
			self.field.validationOptions = [@[@{@(ValidationOptionLengthEqualTo): @18}] mutableCopy];
			[formatter setCountryName:@"Mexico" textField:self.textField];
			break;
		case FieldTypeCard:
			[self.textField becomeFirstResponder];
			self.field.validationOptions = [@[@{@(ValidationOptionLengthEqualTo): @19}] mutableCopy];
			break;
		case FieldTypeExpCard:
			[self.textField becomeFirstResponder];
			self.field.validationOptions = [@[@{@(ValidationOptionLengthEqualTo): @7}] mutableCopy];
			break;
		case FieldTypeURL:
			[self.textField becomeFirstResponder];
			self.field.validationOptions = [@[@(ValidationOptionMustEmail)] mutableCopy];
			
			break;
		case FieldTypeEmail:
			[self.textField becomeFirstResponder];
			break;
		case FieldTypeFloat:
			[self.textField becomeFirstResponder];
			break;
		case FieldTypeString:
			[self.textField becomeFirstResponder];
			break;
		case FieldTypeButtonOptions:
			self.textField.userInteractionEnabled = false;
			[self.superview endEditing:true];
			break;
		default:
			[self.textField becomeFirstResponder];
			break;
	}
	
	self.inputImageView.hidden = false;
	
	//[self.superview layoutIfNeeded];
	[UIView animateWithDuration:0.3 animations:^{
		if (self.errorLbl.hidden == false)
		{
			self.lblSeparationConstraint.constant = 15;
		}
		else
		{
			self.lblSeparationConstraint.constant = 0;
		}
		[self.superview layoutIfNeeded];
	} completion:^(BOOL finished) {
		dispatch_async(dispatch_get_main_queue(), ^{
			if ([self.delegate respondsToSelector:@selector(formInputViewDidStartEditing:)])
			{
				
				dispatch_async(dispatch_get_main_queue(), ^{
					[self.delegate formInputViewDidStartEditing:self];
					
				});
				
			}
		});
	}];
	
}
-(void)endEditing
{
	[self.contentView bringSubviewToFront:self.startButton];
	if ([[[self validate] objectForKey:kFieldSuccessKey] boolValue] == true)
	{
		[self.superview layoutIfNeeded];
		
		[UIView animateWithDuration:0.2 animations:^{
			if (self.textField.text.length == 0)
			{
				self.lblSeparationConstraint.constant = self.textField.frame.size.height * -1;
				self.inputImageView.hidden = true;
				
			}
			[self.superview layoutIfNeeded];
			
		} completion:^(BOOL finished) {
			dispatch_async(dispatch_get_main_queue(), ^{
				if ([self.delegate respondsToSelector:@selector(formInputViewDidEndEditing:)])
				{
					[self.delegate formInputViewDidEndEditing:self];
				}
			});
			
		}];
	}
	
	
	
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
	NSLog(@"touchesEnded");
	
	UITouch *touch = [touches anyObject];
	if (!CGRectContainsPoint(self.frame, [touch locationInView:self]))
	{
		[self.textField resignFirstResponder];
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	// Check textField
	if (self.field.type == FieldTypePhoneNumber)
	{
		return [formatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
	}
	else if (self.field.type == FieldTypeZipCode)
	{
		if (self.textField.text.length + string.length > 5)
		{
			[formatter shakeTextField:textField];
			return false;
		}
	}
	else if (self.field.type == FieldTypeCard)
	{
		return [formatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
		
	}
	else if (self.field.type == FieldTypeExpCard)
	{
		return [formatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
		
	}
	return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	NSLog(@"textFieldShouldReturn");
	shouldGoToNextInput = true;
	[self goToNextInput];
	[self.textField resignFirstResponder];
	return shouldGoToNextInput;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
	NSLog(@"textFieldShouldEndEditing");
	return true;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
	NSLog(@"textFieldDidEndEditing");
	if (shouldGoToNextInput)
	{
		[self goToNextInput];
	}
	[self endEditing];
	
}

-(void)goToNextInput
{
	NSLog(@"goToNextInput");
	shouldGoToNextInput = false;
	if (self.nextInput)
	{
		[self.nextInput startEditing];
	}
	else if (!self.accesoryButton.hidden)
	{
		[self executeAccesoryAction:self.accesoryButton];
	}
}
-(void)setSecureText:(BOOL)secureText
{
	self.textField.font = nil;
	self.textField.font = [UIFont systemFontOfSize:14];
	NSLog(@"%@", self.textField.font);
	tmpPass = self.textField.text;
	_secureText = secureText;
	self.textField.secureTextEntry = secureText;
	self.textField.font = nil;
	self.textField.font = [UIFont systemFontOfSize:14];
}
-(NSDictionary *)validate
{
	self.field.stringValue = self.textField.text;
	self.validationResult = [[self.field validateField] mutableCopy];
	if ([[self.validationResult objectForKey:kFieldSuccessKey] boolValue] == false)
	{
		[self showErrorWithText:[[[self.validationResult objectForKey:kFieldErrorsKey] firstObject] objectForKey:kFieldMessageKey]];
	}
	else
	{
		[self hideError];
	}
	return self.validationResult;
}
-(void)showErrorWithText:(NSString *)text
{
	[self.superview layoutIfNeeded];
	[UIView animateWithDuration:0.2 animations:^{
		self.lblSeparationConstraint.constant = 15;
		self.errorLbl.hidden = false;
		self.errorImageView.hidden = false;
		self.errorLbl.text = text;
		[self.superview layoutIfNeeded];
	}];
	[self shakeTextField];
}
-(void)hideError
{
	[self.superview layoutIfNeeded];
	[UIView animateWithDuration:0.2 animations:^{
		self.lblSeparationConstraint.constant = 0;
		self.errorLbl.hidden = true;
		self.errorImageView.hidden = true;
		
		self.errorLbl.text = @"";
		[self.superview layoutIfNeeded];
	}];
}
- (void)shakeTextField{
	CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
	[shake setDuration:0.1];
	[shake setRepeatCount:2];
	[shake setAutoreverses:YES];
	[shake setFromValue:[NSValue valueWithCGPoint: CGPointMake(self.center.x - 4, self.center.y)]];
	[shake setToValue:[NSValue valueWithCGPoint: CGPointMake(self.center.x + 4, self.center.y)]];
	[self.layer addAnimation:shake forKey:@"position"];
}

+(void)validateInputViews:(NSArray *)inputViews error:(NSError **)error
{
	NSError *inputError;
	for (FormInputView *inputView in inputViews)
	{
		
		[inputView.textField resignFirstResponder];
		[inputView.field validateAndGetError:&inputError];
		if (inputError)
		{
			*error = inputError;
			[inputView showErrorWithText:inputError.localizedDescription];
			if (![inputView isKindOfClass:[SelectableFormInputView class]])
			{
				[inputView startEditing];
			}
			else
			{
				
			}
			return;
		}
	}
}
@end

