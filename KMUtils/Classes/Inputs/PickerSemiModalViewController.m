//
//  SemiModalPickerViewController.m
//  DEQ
//
//  Created by Mauricio Ventura on 15/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "PickerSemiModalViewController.h"
#import "UIColor+Pantone.h"
@interface PickerSemiModalViewController ()

@end

@implementation PickerSemiModalViewController
- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}


- (IBAction)select:(id)sender
{
	if ([self.delegate respondsToSelector:@selector(pickerSemiModalViewController:didSelectStructure:)])
	{
		[self.delegate pickerSemiModalViewController:self didSelectStructure:[self getSelectedStruct]];
	}
	[self dismissWithCompletion:^{
	}];
}

- (IBAction)cancel:(id)sender
{
	
	[self dismissWithCompletion:^{
		if ([self.delegate respondsToSelector:@selector(pickerSemiModalViewControllerDidCancel:)])
		{
			[self.delegate pickerSemiModalViewControllerDidCancel:self];
		}
	}];
}

-(NSArray *)getSelectedStruct
{
	NSMutableArray *tmpArray = [NSMutableArray array];
	if (self.pickerStructure.count == 0)
	{
		tmpArray = [@[self.datePickerView.date] mutableCopy];
	}
	else
	{
		for (int i = 0; i < self.pickerStructure.count; i++)
		{
			[tmpArray addObject:[NSNumber numberWithInteger:[self.pickerView selectedRowInComponent:i]]];
		}

	}
	return tmpArray;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	if (self.pickerStructure)
	{
		return self.pickerStructure.count;
	}
	return 0;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return [[self.pickerStructure objectAtIndex:component] count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	return [[self.pickerStructure objectAtIndex:component] objectAtIndex:row];
}
@end
