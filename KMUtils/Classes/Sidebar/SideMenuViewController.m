//
//  SideMenuViewController.m
//  DEQ
//
//  Created by Mauricio Ventura on 13/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "SideMenuViewController.h"
#import "UIColor+Pantone.h"
#import "UIFont+Fonts.h"
#import "UIView+LoadingAnimations.h"
@interface SideMenuViewController ()

@end

@implementation SideMenuViewController
{
  NSDictionary *options;
  NSMutableArray *menuOptions;
  
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.lastIndex = -1;
  [self getOptions];
  
}

-(void)getOptions
{
    menuOptions = [@[
                     @{
                         @"title": @"HOME",
                         @"storyboardName": @"Main",
                         @"storyboarIdentifier": @"homeNavVC",
                         @"iconImage": @"icn-home"
                         },
                     @{
                         @"title": @"TELECOMUNICACIONES",
                         @"storyboardName": @"Carrier",
                         @"storyboarIdentifier": @"initial",
                         @"iconImage": @"icn-telecom-sidebar"
                         },
                     ] mutableCopy];
  
 
}
-(void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  if (self.tableView.frame.size.height > 300)
  {
    //self.tableView.scrollEnabled = false;
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleLightContent;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  //NSDictionary *menuOption = [menuOptions objectAtIndex:indexPath.row];
  
  return nil;
  
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
}
@end

