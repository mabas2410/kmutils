//
//  SideMenuViewController.h
//  DEQ
//
//  Created by Mauricio Ventura on 13/12/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideBarController.h"
@interface SideMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger lastIndex;
@property (nonatomic, assign) NSInteger ignoreIndex;

@end
