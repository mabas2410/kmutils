//
//  SideBarController.m
//  DEQ
//
//  Created by Mauricio Ventura on 10/11/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import "SideBarController.h"
#import "SideMenuViewController.h"
static const CGFloat kAnimationDuration = 0.3f;

@interface SideBarController ()

@end

@implementation SideBarController
{
	BOOL isShowingMenu;
	UIStatusBarStyle statusBarStyle;
	UIColor *statusbarColor;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	isShowingMenu = false;
	self.contentView.layer.shadowOffset = CGSizeMake(-6, 0);
	self.contentView.layer.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5].CGColor;
	self.contentView.layer.shadowRadius = 12;
	self.contentView.layer.shadowOpacity = 0.7;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.leftMenuLeadingConstraint.constant = self.view.frame.size.width * -1;
	[self.view layoutIfNeeded];
	
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showLeftGesture:(UIScreenEdgePanGestureRecognizer *)sender
{
	switch (sender.state)
	{
  case UIGestureRecognizerStateBegan:
			if (!isShowingMenu)
			{
				[self presentLeftViewController];
			}
			break;
			
  default:
			break;
	}
	
}

- (IBAction)showRightGesture:(UIScreenEdgePanGestureRecognizer *)sender
{
	[self dismissSidebarViewController];
	/*
	switch (sender.state)
	{
  case UIGestureRecognizerStateBegan:
			if (!self.blockRightMenu || isShowingMenu)
			{
				[self presentRightViewController];
			}
			break;
			
  default:
			break;
	}
	*/
}

-(void)presentLeftViewController
{
	UINavigationController  *navVC;
	for (id vc in self.childViewControllers)
	{
		if ([vc isKindOfClass:[UINavigationController class]])
		{
			navVC = vc;
		}
	}
	if (navVC.viewControllers.count == 1)
	{
		[self.view endEditing:true];
		if (self.contentHorizontalConstraint.constant == 0)
		{
			[[NSNotificationCenter defaultCenter] postNotificationName:@"GetUnreads" object:nil];
			[self.view layoutIfNeeded];
			[UIView animateWithDuration:kAnimationDuration animations:^{
				self.leftMenuLeadingConstraint.constant = 0;
				
				/*El cMainse mueve
				 self.contentHorizontalConstraint.constant = self.leftBarView.frame.size.width;
				 */
				
				//self.blockerView.hidden = false;
				self.blockerView.alpha = 1;
				[self.view layoutIfNeeded];

			} completion:^(BOOL finished) {

			}];
			isShowingMenu = true;
			[self topView];
			[self preferredStatusBarStyle];
		}
		else
		{
			[self dismissSidebarViewController];
		}
	}
}
-(void)presentRightViewController
{
	if (self.contentHorizontalConstraint.constant == 0)
	{
		[self.view layoutIfNeeded];
		[UIView animateWithDuration:kAnimationDuration animations:^{

			self.contentHorizontalConstraint.constant = self.rightBarView.frame.size.width * -1;
			[self.view layoutIfNeeded];

		}];
		isShowingMenu = true;
	}
	else
	{
		[self dismissSidebarViewController];
	}
}
-(void)dismissSidebarViewController
{
	[self replaceContentViewControllerWith:nil];
	[self preferredStatusBarStyle];
}
-(void)replaceContentViewControllerWith:(id)newVC
{
	if (newVC)
	{
		
		
		
		self.contentNavigationController = newVC;
		//self.contentNavigationController.viewControllers = @[newVC];
		//[self presentViewController:self.contentNavigationController animated:true completion:nil];
		
	}
	[self.view layoutIfNeeded];
	[UIView animateWithDuration:kAnimationDuration * 0.7 animations:^{
		//self.contentHorizontalConstraint.constant = 0;
		self.leftMenuLeadingConstraint.constant = self.view.frame.size.width * -1;
		//self.blockerView.hidden = true;
		self.blockerView.alpha = 0;
		[self.view layoutIfNeeded];

	}];
	isShowingMenu = false;
}

-(void)setContentNavigationController:(UINavigationController *)contentNavigationController
{
	
	[self.contentNavigationController.view removeFromSuperview];
	[self.contentNavigationController willMoveToParentViewController:nil];
	[self.contentNavigationController removeFromParentViewController];
	
	
	[self.contentView addSubview:contentNavigationController.view];
	[self addChildViewController:contentNavigationController];
	
	
}
-(UIView *)topView
{
	UIView *topView;
	for (UIViewController *vc in self.childViewControllers)
	{
		if (vc.view.frame.size.width > topView.frame.size.width)
		{
			topView = vc.view;
		}
	}
	UIView *topView1;
	for (UIView *view in topView.subviews)
	{
		if (view.frame.size.height > topView1.frame.size.height)
		{
			topView1 = view;
		}
	}
	UIView *topView2;
	for (UIView *view in topView1.subviews.firstObject.subviews)
	{
		if (view.frame.size.height > topView2.frame.size.height)
		{
			topView2 = view;
		}
	}
	return topView;
}
@end

#pragma mark - SidebarController Category
@implementation UIViewController(SideBarController)
@dynamic contentNavigationController;

- (SideBarController *)sidebarController
{
	__weak UIViewController *parent = self.parentViewController;
	if (!parent)
	{
		parent = self.navigationController;
	}
	while (parent && ![parent isKindOfClass:[SideBarController class]])
	{
		parent = parent.parentViewController;
		if (!parent)
		{
			//parent = self.navigationController;
		}
	}
	if ([parent isKindOfClass:[SideBarController class]])
	{
		return (SideBarController *)parent;
	}
	return nil;
}

- (UINavigationController *)contentNavigationController
{
	for (id vc in self.childViewControllers)
	{
		if ([vc isKindOfClass:[UINavigationController class]])
		{
			return  (UINavigationController *) vc;
		}
	}
	return nil;
}
- (UINavigationController *)leftViewController
{
	for (__weak UIViewController *vc in self.childViewControllers)
	{
		if ([vc.restorationIdentifier containsString:@"eftSidebarVC"])
		{
			return (UINavigationController *)vc;
		}
	}
	return nil;
}
- (UINavigationController *)rightViewController
{
	for (__weak UIViewController *vc in self.childViewControllers)
	{
		if ([vc.restorationIdentifier isEqualToString:@"rightSidebarVC"])
		{
			return (UINavigationController *)vc;
		}
	}
	return nil;
}

@end
