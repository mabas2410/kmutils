//
//  SideBarController.h
//  DEQ
//
//  Created by Mauricio Ventura on 10/11/16.
//  Copyright © 2016 ezapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideBarController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *leftBarView;
@property (weak, nonatomic) IBOutlet UIView *rightBarView;
@property (weak, nonatomic) IBOutlet UIView *blockerView;

@property (weak) UIViewController *leftViewController;

@property (weak) UIViewController *rightViewController;

@property BOOL blockRightMenu;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHorizontalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftMenuLeadingConstraint;

- (IBAction)showLeftGesture:(id)sender;
- (IBAction)showRightGesture:(id)sender;


-(void)presentLeftViewController;
-(void)presentRightViewController;
-(void)dismissSidebarViewController;
-(void)replaceContentViewControllerWith:(id)newVC;


@end
@interface UIViewController(SideBarController)

@property (weak, readonly, nonatomic) SideBarController *sidebarController;

@property (weak, nonatomic) UINavigationController *contentNavigationController;

@property (weak, readonly, nonatomic) UIViewController *leftViewController;

@property (weak, readonly, nonatomic) UIViewController *rightViewController;
@end
