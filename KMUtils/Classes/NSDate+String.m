//
//  NSDate+String.m
//  Kmabas
//
//  Created by Mauricio Ventura on 11/10/16.
//  Copyright © 2016 Kmabas. All rights reserved.
//

#import "NSDate+String.h"

@implementation NSDate (String)
-(NSString *)getHumanReadableStringWithFormat:(NSString *)format
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:format];
	return [formatter stringFromDate:self];
}
-(NSString *)getServerParamString
{
	NSString *fecha = [self getHumanReadableStringWithFormat:@"yyyy-MM-dd"];
	return fecha;
}
-(NSString *)getYearString
{
	NSString *fecha = [self getHumanReadableStringWithFormat:@"yyyy"];
	return fecha;
}
-(NSString *)getMonthName
{
	NSString *fecha = [self getHumanReadableStringWithFormat:@"MMMM"];
	return fecha;
}
-(NSInteger)getMonthInteger
{
	NSString *fecha = [self getHumanReadableStringWithFormat:@"MM"];
	return [fecha integerValue];
}

-(NSString *)getMonthAndYear
{
	NSString *fecha = [self getHumanReadableStringWithFormat:@"MM-yyyy"];
	return fecha;
}
+(NSDate *)dateFromString:(NSString *)dateStr withFormat:(NSString *)format
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:format];
	return [formatter dateFromString:dateStr];
}

-(NSDate*)lastDayOfMonth
{
	NSInteger dayCount = [self numberOfDaysInMonthCount];
	
	NSDateComponents *comp = [[self calendar] components:
														NSCalendarUnitYear |
														NSCalendarUnitMonth |
														NSCalendarUnitDay fromDate:self];
	
	[comp setDay:dayCount];
	
	return [[self calendar] dateFromComponents:comp];
}

-(NSInteger)numberOfDaysInMonthCount
{
	NSRange dayRange = [[self calendar] rangeOfUnit:NSCalendarUnitDay
																					 inUnit:NSCalendarUnitMonth
																					forDate:self];
	
	return dayRange.length;
}
-(NSString *)getPropperDifference
{
	NSString *unit = @"seg";
	NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:self];
	NSInteger magnitud = time;

	if (magnitud > 60)
	{
		magnitud = round(magnitud / 60);
		unit = @"min";
		if (magnitud > 60)
		{
			unit = @"h";
			magnitud = round(magnitud / 60);
			if (magnitud > 24)
			{
				unit = @"d";
				magnitud = round(magnitud / 24);
				if (magnitud > 7)
				{
					unit = @"sem";
					magnitud = round(magnitud / 7);
					if (magnitud > 4)
					{
						unit = @"m";
						magnitud = round(magnitud / 4);
						if (magnitud > 12)
						{
							unit = @"y";
							magnitud = round(magnitud / 12);
						}
					}
					
				}
			}
		}
	}
	return [NSString stringWithFormat:@"%ld%@", (long)magnitud, unit];

	
}

-(NSCalendar*)calendar
{
	NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
	return calendar;
}
@end
