//
//  KMViewController.m
//  KMUtils
//
//  Created by Mauricio Ventura on 10/27/2017.
//  Copyright (c) 2017 Mauricio Ventura. All rights reserved.
//

#import "KMViewController.h"
#import <KMUtils/NSDate+String.h>

@interface KMViewController ()

@end

@implementation KMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	Field *testField = [[Field alloc] initWithDict:@
													{
														@"type": @(FieldTypeDate),
													kFieldKeyKey:@"birthdate",
													kFieldOptionsKey: @[@(ValidationOptionPast)]
														
													}];
	[self.testInput setField:testField];
	self.testInput.transitionDelegate = self;
	self.testInput.delegate = self;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)formInputViewDidStartEditing:(FormInputView *)formInput
{
	
}
-(void)formInputView:(FormInputView *)formInput accesoryButtonDidPressed:(UIButton *)sender
{
	
}
-(void)formInputViewDidEndEditing:(FormInputView *)formInput
{
	if ([formInput.field.paramKey isEqualToString:@"birthdate"]) {
		[NSLocale localeWithLocaleIdentifier:NSLocaleCalendar];
		NSDate* now = [NSDate dateFromString:formInput.field.stringValue withFormat:((SelectableFormInputView *)formInput).joinString];
		NSDateFormatter* df = [[NSDateFormatter alloc] init];
		[df setDateStyle:NSDateFormatterLongStyle];
		[df setTimeStyle:NSDateFormatterNoStyle];
		NSString* myString = [df stringFromDate:now];
		formInput.textField.text = myString;
	}
}

-(void)formInputView:(SelectableFormInputView *)formInput shouldPresentPicker:(PickerSemiModalViewController *)semimodalPicker
{
	[self.view addSubview:semimodalPicker.view];
	[self addChildViewController:semimodalPicker];
	[self.view layoutIfNeeded];
	[UIView animateWithDuration:0.7 animations:^{
		semimodalPicker.view.center = CGPointMake(semimodalPicker.view.center.x, self.view.frame.size.height / 2);
		[semimodalPicker present];
		[self.view layoutIfNeeded];
	} completion:^(BOOL finished)
	 {
	 }];
}


-(void)formInputView:(SelectableFormInputView *)formInput shouldDismissPicker:(PickerSemiModalViewController *)picker
{
	NSLog(@"%@", formInput.textField.text);
}



@end
