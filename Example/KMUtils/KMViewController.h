//
//  KMViewController.h
//  KMUtils
//
//  Created by Mauricio Ventura on 10/27/2017.
//  Copyright (c) 2017 Mauricio Ventura. All rights reserved.
//

@import UIKit;
#import <KMUtils/SelectableFormInputView.h>
@interface KMViewController : UIViewController<SelectableFormInputViewTransitionDelegate, FormInputViewDelegate>
@property (weak, nonatomic) IBOutlet SelectableFormInputView *testInput;

@end
