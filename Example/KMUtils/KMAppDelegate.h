//
//  KMAppDelegate.h
//  KMUtils
//
//  Created by Mauricio Ventura on 10/27/2017.
//  Copyright (c) 2017 Mauricio Ventura. All rights reserved.
//

@import UIKit;

@interface KMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
