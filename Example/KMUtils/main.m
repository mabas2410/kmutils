//
//  main.m
//  KMUtils
//
//  Created by Mauricio Ventura on 10/27/2017.
//  Copyright (c) 2017 Mauricio Ventura. All rights reserved.
//

@import UIKit;
#import "KMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KMAppDelegate class]));
    }
}
